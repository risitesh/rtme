<?php
session_start();
if(!isset($_SESSION['login_id'])){
	header("location:index.php");

}
include "includes/connect.php";
?>
<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Admin Area | Recent Trends In Mechanical Engineering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
		
		

	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<!-- MAIN -->
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
						<!-- title -->
						<div id="page-title">
							<span class="title">Welcome</span>
							<span class="subtitle">Admin RTME - 2015</span>
							<span class="subtitle"><a href="logout.php">Logout</a></span>
						</div>
						<!-- ENDS title -->
		
						<!-- page-content -->
						<div id="page-content">
						
							<!-- TABS -->
							<!-- the tabs -->
							<ul class="tabs">
								<li><a href="#"><span>Abstract Paper</span></a></li>
								<li><a href="#"><span>Full Paper</span></a></li>
								<li><a href="#"><span>Camera Ready Paper</span></a></li>
								<li><a href="#"><span>Final List</span></a></li>
							</ul>
							
							<!-- tab "panes" -->
							<div class="panes">
							
								<!-- tab 1 content  -->
								<div>
									<?php
										$q = mysql_query("SELECT * FROM paper WHERE ab_approve=0 AND full_approve=0 AND camera_approve=0") or die(mysql_error());
										if(mysql_num_rows($q)!=0){
											echo "<table>
										<tr>
										<th>Reference No.</th>
										<th>Name</th><th>Father's Name</th><th>Qualification</th><th>Designation</th><th>Abstract</th><th>Detail</th><th>Approve</th>
										</tr>";
											while($row = mysql_fetch_assoc($q)):
									?>
										<tr>
											<td><?php echo $row['id']?></td>
											<td><?php echo $row['name']?></td>
											<td><?php echo $row['father']?></td>
											<td><?php echo $row['qualification']?></td>
											<td><?php echo $row['designation']?></td>
											<td><a href="abstract/<?php echo $row['abs_code']?>/<?php echo $row['abstract']?>" target=_blank>Abstract</a></td>
											<td><a href="detail.php?id=<?php echo $row['id']?>" target=_blank>Detail</a></td>
											<td><a href="approve.php?id=<?php echo $row['id']?>">Approve</a></td>
										</tr>
									<?php
										endwhile;
										echo "</table>";
										}else{
											echo "No Abstract Paper Present";
										}
									?>
								</div>
								<!-- ENDS tab 1 content -->
								
								<!-- tab 2 content  -->
								<div>
									<?php
										$q = mysql_query("SELECT * FROM paper WHERE ab_approve=1 AND full_approve=0 AND camera_approve=0") or die(mysql_error());
										if(mysql_num_rows($q)!=0){
											echo "<table>
										<tr>
										<th>Reference No.</th>
										<th>Name</th><th>Father's Name</th><th>Qualification</th><th>Designation</th><th>Full Paper</th><th>Detail</th><th>Approve</th>
										</tr>";
											while($row = mysql_fetch_assoc($q)):
									?>
										<tr>
											<td><?php echo $row['id']?></td>
											<td><?php echo $row['name']?></td>
											<td><?php echo $row['father']?></td>
											<td><?php echo $row['qualification']?></td>
											<td><?php echo $row['designation']?></td>
											<td><?php if($row['full']!=""){
												?>
												<a href="fullpaper/<?php echo $row['full_code']?>/<?php echo $row['full']?>" target=_blank>Full Paper</a>
												<?php }else{?>
												Not Given
												<?php }?>
											</td>
											<td><a href="detail.php?id=<?php echo $row['id']?>">Detail</a></td>
											<td><?php if($row['full']!=""){
												?>
											<a href="approve1.php?id=<?php echo $row['id']?>">Approve</a>
											<?php }else{?>
												Can't Approve
												<?php }?>
											</td>
										</tr>
									<?php
										endwhile;
										echo "</table>";
										}else{
											echo "No Full Paper Present";
										}
									?>            
								</div>
								<!-- ENDS tab 2 content -->
								
								<!-- tab 3 content  -->
								<div>
									<?php
										$q = mysql_query("SELECT * FROM paper WHERE ab_approve=1 AND full_approve=1 AND camera_approve=0") or die(mysql_error());
										if(mysql_num_rows($q)!=0){
											echo "<table>
										<tr>
										<th>Reference No.</th>
										<th>Name</th><th>Father's Name</th><th>Qualification</th><th>Designation</th><th>Camera Paper</th><th>Copyright</th><th>Detail</th><th>Approve</th>
										</tr>";
											while($row = mysql_fetch_assoc($q)):
									?>
										<tr>
											<td><?php echo $row['id']?></td>
											<td><?php echo $row['name']?></td>
											<td><?php echo $row['father']?></td>
											<td><?php echo $row['qualification']?></td>
											<td><?php echo $row['designation']?></td>
											<td><?php if($row['camera']!=""){
												?>
												<a href="camerapaper/<?php echo $row['camera_code']?>/<?php echo $row['camera']?>" target=_blank>Camera Paper</a>
												<?php }else{?>
												Not Given
												<?php }?>
											</td>
											<td><?php if($row['copyright']!=""){
												?>
												<a href="copyright/<?php echo $row['copycode']?>/<?php echo $row['copyright']?>" target=_blank>Copyright</a>
												<?php }else{?>
												Not Given
												<?php }?>
											</td>
											<td><a href="detail.php?id=<?php echo $row['id']?>">Detail</a></td>
											<td><?php if($row['camera']!=""){
												?>
											<a href="approve2.php?id=<?php echo $row['id']?>">Approve</a>
											<?php }else{?>
												Can't Approve
												<?php }?>
											</td>
										</tr>
									<?php
										endwhile;
										echo "</table>";
										}else{
											echo "No Camera Ready Paper Present";
										}
									?>            
								</div>
								<!-- ENDS tab 3 content -->

								<!-- Tab 4 Content -->
								<div>
									<?php
										$q = mysql_query("SELECT * FROM paper WHERE ab_approve=1 AND full_approve=1 AND camera_approve=1") or die(mysql_error());
										if(mysql_num_rows($q)!=0){
											echo "<table>
										<tr>
										<th>Reference No.</th>
										<th>Name</th><th>Abstract</th><th>Full Paper</th><th>Camera Paper</th><th>Detail</th>
										</tr>";
											while($row = mysql_fetch_assoc($q)):
									?>
										<tr>
											<td><?php echo $row['id']?></td>
											<td><?php echo $row['name']?></td>
											<td><a href="abstract/<?php echo $row['abs_code']?>/<?php echo $row['abstract']?>" target=_blank>Abstract</a></td>
											<td><a href="fullpaper/<?php echo $row['full_code']?>/<?php echo $row['full']?>" target=_blank>Full Paper</a></td>
											<td><a href="camerapaper/<?php echo $row['camera_code']?>/<?php echo $row['camera']?>" target=_blank>Camera Paper</a></td>
											<td><a href="detail.php?id=<?php echo $row['id']?>">Detail</a></td>
										</tr>
									<?php
										endwhile;
										echo "</table>";
										}else{
											echo "No Record Present";
										}
									?>            
								</div>
								<!-- End Tab 4 -->								
							</div>
							<!-- ENDS TABS -->
		
		
						</div>
						<!-- ENDS page-content -->
		
					</div>
					<!-- ENDS content -->
				</div>
				<!-- ENDS wrapper-main -->
			</div>
			<!-- ENDS MAIN -->
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>