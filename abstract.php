<?php
include "includes/connect.php";
$msg='';
if(isset($_POST['submit'])){
	$name = mysql_real_escape_string($_POST['name']);
	$father = mysql_real_escape_string($_POST['father']);
	$exp = mysql_real_escape_string($_POST['experience']);
	$qual = mysql_real_escape_string($_POST['qualification']);
	$aff = mysql_real_escape_string($_POST['affilation']);
	$occ = mysql_real_escape_string($_POST['designation']);
	$email = mysql_real_escape_string($_POST['email']);
	$phone = mysql_real_escape_string($_POST['phone']);

	$abname=$_FILES["abstract"]["name"];
    $temp=$_FILES["abstract"]["tmp_name"];
    $size=$_FILES["abstract"]["size"];
    $charset="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $length=15;
    $code="";
    for ($i=0; $i<=$length; $i++)
      {
        $rand= rand() % strlen($charset);
        $tmp=  substr($charset, $rand,1);
        $code .=$tmp;
      }
       $query=mysql_query("INSERT INTO paper(name,father,qualification,affilation,designation,experience,email,phone,abstract,abs_code) VALUES('$name','$father','$qual','$aff','$occ','$exp','$email','$phone','$abname','$code')") or die(mysql_error());
        mkdir("abstract/$code"); 
       $upload= move_uploaded_file($temp,"abstract/$code/".$abname);
       if($query && $upload){
           $msg= "Abstract uploaded sucessfully. Your Reference ID is".mysql_insert_id();
           $msg_type = "success";
     }else{
     	die(mysql_error());
     }
}
?>
<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Submit Your Abstract | Recent Trends In Mechanical Enginnering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
		
		

	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
					<!-- title -->
					<div id="page-title">
						<span class="title">Submit Your Abstract</span>
					</div>
					<!-- ENDS title -->

					<div class="one-column">
						<?php
					if (isset($_POST['submit']) && $msg!="") {
						$output = '<center><span style="text-decoration:underline; font-size:125%;';
						switch ($msg_type) {
							case "error" :
							$output .= "color: red;";
							$output .= '"><b>Error: </b>';
							break;
							case "success" :
							$output .= "color: green;";
							$output .= '">';
							break;
							case "info" :
							$output .= "color: #CF0;";
							$output .= '"><b>Info: </b>';
							break;
						}
						$output .= $msg.'</span></center><br />';
						echo $output;
					}
			?>
							<?php include "includes/guide.php";?><br/>
							<a href='https://drive.google.com/file/d/0B8RWbNFg7HVQRjdnXzdyTkF3VVl4NVR4SzcxdVBBUDZseDlJ/view?usp=sharing' target=_blank><b style='color:red'>Format For Abstract</b></a>
							<!-- form -->
							<h2 style="margin-top:15px">Fill Your Details</h2>
							<form id="contactForm" action="abstract.php" method="post" enctype="multipart/form-data">
								<fieldset>
									<div>
										<label>Name</label>
										<input name="name"  id="name" type="text" class="form-poshytip" title="Enter your full name" required />
									</div>
									<div>
										<label>Father's Name</label>
										<input name="father" type="text" class="form-poshytip" title="Enter your Father's Name" required />
									</div>
									<div>
										<label>Email</label>
										<input name="email"  id="email" type="text" class="form-poshytip" title="Enter your email address" required />
									</div>
									<div>
										<label>Contact No.</label>
										<input name="phone" type="text" class="form-poshytip" title="Enter your Contact No" required />
									</div>
									<div>
										<label>Qualification</label>
										<input name="qualification" type="text" class="form-poshytip" title="Enter your Qualification" required/>
									</div>
									<div>
										<label>Affilation</label>
										<input name="affilation" type="text" class="form-poshytip" title="Enter your Affilation" required />
									</div>
									<div>
										<label>Occupation/ Designation</label>
										<input name="designation" type="text" class="form-poshytip" title="Enter your occupation/ Designation" required />
									</div>
									<div>
										<label>Experience</label>
										<textarea  name="experience" rows="5" cols="20" class="form-poshytip" title="Enter your Experience" required ></textarea>
									</div>
									<div>
										<label>Upload Abstract</label>
										<input name="abstract" type="file" title="Enter your Abstract" required />
									</div>								
									<p><input type="submit" value="SEND" name="submit" id="submit" /></p>
								</fieldset>
							</form>
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>