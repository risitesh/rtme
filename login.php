<?php
include "includes/connect.php";
$msg = "";
	if(isset($_POST['submit'])){
	$user=mysql_real_escape_string($_POST["username"]);
	$pass=mysql_real_escape_string($_POST["password"]);
								
	$result=mysql_query("SELECT * FROM login WHERE username='$user' AND password='$pass'") or die(mysql_error());
	$count=mysql_num_rows($result);
	$row=mysql_fetch_array($result);
								
	if($count>0)
		{
			session_start();
			$_SESSION["login_id"]=$row["login_id"];
			header("location:rtme-admin.php");
		}
	else
		{
			$msg='Invalid Username/Password';
			$msg_type='error';
		}
	}
?>
<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Admin Login | Recent Trends In Mechanical Enginnering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
		
		

	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
					<!-- title -->
					<div id="page-title">
						<span class="title">Login</span>
					</div>
					<!-- ENDS title -->

					<div class="one-column">
							<!-- form -->
							<form id="contactForm" action="login.php" method="post">
								<fieldset>
									<?php
					if (isset($_POST['submit']) && $msg!="") {
						$output = '<center><span style="text-decoration:underline; font-size:125%;';
						switch ($msg_type) {
							case "error" :
							$output .= "color: red;";
							$output .= '"><b>Error: </b>';
							break;
							case "success" :
							$output .= "color: green;";
							$output .= '">';
							break;
							case "info" :
							$output .= "color: #CF0;";
							$output .= '"><b>Info: </b>';
							break;
						}
						$output .= $msg.'</span></center><br />';
						echo $output;
					}
			?>
									<div>
										<label>Username.</label>
										<input name="username" type="text" class="form-poshytip" title="Enter your Username." required />
									</div>	
									<div>
										<label>Password</label>
										<input name="password" type="password" class="form-poshytip" title="Enter your Password" required />
									</div>			
									<p><input type="submit" value="Login" name="submit" id="submit" /></p>
								</fieldset>
							</form>
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>