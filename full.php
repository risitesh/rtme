<?php
include "includes/connect.php";
session_start();
$msg="";
?>
<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Submit Your Full Paper | Recent Trends In Mechanical Engineering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
		
		

	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
					<!-- title -->
					<div id="page-title">
						<span class="title">Submit Your Full Paper</span>
					</div>
					<!-- ENDS title -->

					<div class="one-column">
							<?php include "includes/guide.php";?><br/>
							<a href='https://drive.google.com/file/d/0B8RWbNFg7HVQMlZwU0w1S1c1NkEybWZxV3lHc0dlbXVmSXRv/view?usp=sharing' target=_blank><b style='color:red'>Format For Paper</b></a>
							<!-- form -->
							<h2 style="margin-top:15px">Enter Your Reference Number</h2>
							<form id="contactForm" action="full.php" method="POST">
								<fieldset>
									<div>
										<input name="reference" type="text" class="form-poshytip" title="Enter your reference number" required />
									</div>
									<!-- <div>
										<label>Upload Abstract</label>
										<input name="abstract" type="file" title="Enter your Abstract" required />
									</div> -->								
									<p><input type="submit" value="Submit" name="submit" id="submit" /></p>
								</fieldset>
							</form>
							<?php
							if(isset($_POST['submit'])){
								$ref = mysql_real_escape_string($_POST['reference']);
								$_SESSION['ref'] = $ref;
								$q = mysql_query("SELECT * FROM paper WHERE id=".$ref) or die(mysql_error());
								if(mysql_num_rows($q)!=0){
									$qr = mysql_query("SELECT * FROM paper WHERE ab_approve=1 AND full_approve=0 AND id=".$ref) or die(mysql_error());
									if(mysql_num_rows($qr)!=0){
										$qre = mysql_query("SELECT * FROM paper WHERE full_submit=0 AND id=".$ref) or die(mysql_error());
										if(mysql_num_rows($qre)!=0){
										while ($row = mysql_fetch_assoc($qr)):
										?>
									<table>
										<tr>
										<th>Name</th><th>Father's Name</th><th>Qualification</th><th>Affilation</th><th>Designation</th><th>Experience</th><th>Email ID</th><th>Contact No</th>
										</tr>
										<tr>
											<td><?php echo $row['name']?></td>
											<td><?php echo $row['father']?></td>
											<td><?php echo $row['qualification']?></td>
											<td><?php echo $row['affilation']?></td>
											<td><?php echo $row['designation']?></td>
											<td><?php echo $row['experience']?></td>
											<td><?php echo $row['email']?></td>
											<td><?php echo $row['phone']?></td>
										</tr>
									</table>
									<?php
									endwhile;
									?>
									<form id="contactForm" action="full.php" method="post" enctype="multipart/form-data">
									<fieldset>
									<div>
										<label>Upload Full Paper</label>
										<input name="full" type="file" title="Enter your Abstract" required />
									</div>
									<p><input type="submit" value="SEND" name="send" id="submit" /></p>
									</fieldset>
									</form>
									<?php
								}else{
									echo "Already Given Full Paper";
								}
									}else{
										echo "Abstract Not Apprroved Yet";
									}
								}else{
									echo "Reference Number Not Found";
								}
							}
							?>
							<?php
								if(isset($_POST['send'])){
									$abname=$_FILES["full"]["name"];
    								$temp=$_FILES["full"]["tmp_name"];
								    $size=$_FILES["full"]["size"];
								    $charset="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
								    $length=15;
								    $code="";
								    for ($i=0; $i<=$length; $i++)
								      {
								        $rand= rand() % strlen($charset);
								        $tmp=  substr($charset, $rand,1);
								        $code .=$tmp;
								      }
								       $query=mysql_query("UPDATE paper SET full='$abname', full_code='$code', full_submit=1 WHERE id=".$_SESSION['ref']) or die(mysql_error());
								        mkdir("fullpaper/$code"); 
								       $upload= move_uploaded_file($temp,"fullpaper/$code/".$abname);
								       if($query && $upload){
								           echo "Full Paper uploaded sucessfully. We'll Contact You Soon";
								     }else{
								     	die(mysql_error());
								     }
								}
							?>
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>