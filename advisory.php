<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>National Advisory Committee | Recent Trends In Mechanical Enginnering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
					<!-- title -->
					<div id="page-title">
						<span class="title">National Advisory Committee</span>
					</div>
					<!-- ENDS title -->

					<div class="one-column">
							Dr. Buta Singh, Dean Academic, PTU Jalandhar<br/>
							Dr. A.P. Singh, Dean RIC, PTU Jalandhar<br/>
							Dr. I.K. Bhatt, Director, MNIT Jaipur<br/>
							Dr. H.S. Hundal, Director, GZSPTUC Bathinda<br/>
							Dr. T.S. Sidhu, Director, SBSTC Ferozepur<br/>
							Dr. H.S. Bains, Director, PUSSGRC, Hoshiarpur<br/>
							Dr. J.K. Sharma, Director, ACEAR, Ambala<br/>
                                                        Dr. H.S. Shan, Ex. Prof. IIT Roorkee & Advisor PIT Mohali<br/>
							Dr. P.V. Rao, IIT Delhi<br/>
							Dr. O.P. Gandhi, IIT Delhi<br/>
							
							Dr. P.K. Jain, IIT Roorkee<br/>
							Dr. Sehijpal Singh, GNDEC Ludhiana<br/>
							Dr. Harpreet Singh, IIT Ropar<br/>
							Dr. I.P. Singh, IIT Roorkee<br/>
							Dr. Subash Chardra, NIT Jalandhar<br/>
							Dr. S. Maheshwari, NSIT Delhi<br/>
							Dr. Vishal S. Sharma, NIT Jalandhar<br/>
							Dr. Rajesh Sharma, NIT Hamirpur<br/>
							Dr. Vishavjit Singh, PAU Ludhiana<br/>
							Dr. S. Thakkar, PEC University Chandigarh<br/>
							Dr. Rupinder Singh, GNDEC Ludhiana<br/>
							Dr. R.S. Walia, DTU Delhi
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>