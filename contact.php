<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Contact Us | Recent Trends In Mechanical Enginnering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
					<!-- title -->
					<div id="page-title">
						<span class="title">Contact Us</span>
					</div>
					<!-- ENDS title -->

					<div class="one-third">
					<h6><u>CONFERENCE CHAIR</u></h6>
					<b>
						Dr.Dilbag Singh, HOD<br/>
						Department Of Mechanical Engineering,<br/>
						Beant College Of Engineering & Technology,<br/>
						Gurdaspur - 143521. Punjab, INDIA<br/>
						Contact No. : +918872458883<br/>
						Email ID : <u>rtme2015bcet@gmail.com</u>
					</b>
					</div>
					<div class="one-third">
					<h6><u>ORGANIZING SECRETARY</u></h6>
					<b>
						Dr. Harish Pungotra, Associate Professor<br/>
						Department Of Mechanical Engineering,<br/>
						Beant College Of Engineering & Technology,<br/>
						Gurdaspur - 143521. Punjab, INDIA<br/>
						Contact No. : +918872733343<br/>
						Email ID : <u>rtme2015bcet@gmail.com</u><br/><br/>
						Dr. Nirmal S Kalsi, Associate Professor<br/>
						Department Of Mechanical Engineering,<br/>
						Beant College Of Engineering & Technology,<br/>
						Gurdaspur - 143521. Punjab, INDIA<br/>
						Contact No. : +918872500370<br/>
						Email ID : <u>rtme2015bcet@gmail.com</u><br/>
					</b>
					</div>
					<div class="one-third last">
					<h6><u>COLLEGE</u></h6>
					<b>
						Beant College Of Engineering & Technology,<br/>
						Gurdaspur - 143521. Punjab, INDIA<br/>
						Tel : 01874-221463 Fax : 01874-221464<br/>
						Email ID : <u>rtme2015bcet@gmail.com</u>
					</b>
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>