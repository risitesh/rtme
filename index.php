<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Recent Trends In Mechanical Engineering (RTME - 2015) | BCET Gurdaspur</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="engine1/style.css" />
		<script type="text/javascript" src="engine1/jquery.js"></script>
		<!-- JS -->
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
	</head>
	
	<body class="home">

			<!-- HEADER -->
			<?php include "includes/header.php"; ?>
			<!-- Navigation -->
			<?php include "includes/nav.php";?>			
<br/>
			<!-- Slider -->
			<div id="wowslider-container1">
	<div class="ws_images"><ul>
<li><img src="data1/images/2.jpg" alt="" title="" id="wows1_0"/></li>
<li><img src="data1/images/7.jpg" alt="" title="" id="wows1_1"/></li>
<li><img src="data1/images/5.jpg" alt="" title="" id="wows1_2"/></li>
<li><img src="data1/images/6.jpg" alt="" title="" id="wows1_3"/></li>
<li><img src="data1/images/9.jpg" alt="" title="" id="wows1_4"/></li>
<li><img src="data1/images/10.jpg" alt="" title="" id="wows1_5"/></li>
<li><a href="http://wowslider.net"><img src="data1/images/8.jpg" alt="slideshow" title="" id="wows1_6"/></a></li>
<li><img src="data1/images/11.jpg" alt="" title="" id="wows1_7"/></li>
</ul></div>
<div class="ws_bullets"><div>
<a href="#" title=""><img src="data1/tooltips/2.jpg" alt=""/>1</a>
<a href="#" title=""><img src="data1/tooltips/7.jpg" alt=""/>2</a>
<a href="#" title=""><img src="data1/tooltips/5.jpg" alt=""/>3</a>
<a href="#" title=""><img src="data1/tooltips/6.jpg" alt=""/>4</a>
<a href="#" title=""><img src="data1/tooltips/9.jpg" alt=""/>5</a>
<a href="#" title=""><img src="data1/tooltips/10.jpg" alt=""/>6</a>
<a href="#" title=""><img src="data1/tooltips/8.jpg" alt=""/>7</a>
<a href="#" title=""><img src="data1/tooltips/11.jpg" alt=""/>8</a>
</div></div>
<div class="ws_shadow"></div>
	</div>	
	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
			<!-- ENDS Slider -->
			
			<!-- MAIN -->
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					<!-- headline -->
					<div class="clear"></div>					
					<!-- content -->
					<div id="content">
						<br/>
							<!-- TABS -->
							<!-- the tabs -->
							<ul class="tabs">
								<li><a href="#"><span>About BCET Gurdaspur</span></a></li>
								<li><a href="#"><span>Department</span></a></li>
								<li><a href="#"><span>About Conference Details</span></a></li>
							</ul>
							
							<!-- tab "panes" -->
							<div class="panes">
							
								<!-- Posts -->
								<div style="font-size:15px">
								<div class="plain-text">
                                                                    Beant College Of Engineering & Technology, Gurdaspur was established by the Government of Punjab 1995. The institute is committed to impart to quality education to the students. The College is running 10+1, 10+2, Diploma, Under-Graduate and Post-Graduate courses in Engineering. The BCET Campus, with its lush green state-of-the-art campus spread over 70 acres is situated on Gurdaspur-Pathankot Road, about 4 km away from the Gurdaspur Bus Stand. <br/><br/>
								    <b>NEARBY PLACES TO VISIT</b><br/><br/>
                                                                                      Golden Temple, Amritsar(75Km)<br/>
                                                                                      Durgiana Mandir(75Km)<br/>
                                                                                      Jallianwala Bhag(75Km)<br/>
                                                                                      Wagha Border along Pakistan, Amritsar(85Km)<br/>
                                                                                      Dharmshala, H.P. (100Km)<br/>
                                                                                      Dalhousie, H.P. (110Km)<br/>
                                                                                      Vaishnodevi Shrive , J&K (150Km)<br/>

                                                                
                                                                </div>
								</div>
								<!-- ENDS posts -->
								
								<!-- Information  -->
								<div>
									<div class="plain-text">
										 The Department Of Mechanical Engineering Offers four years B.Tech course in Mechanical Engineering and M.Tech program in Thermal and Production Engineering and equipped with a latest infrastructure facilities. 	
									        
                                                                        </div>
								</div>
								<!-- ENDS Information -->

								<!-- Posts -->
								<div>
									<div class="plain-text">
                                                                            The 2nd National Conference on Recent Trends in Mechanical Engineering (RTME - 2015) is in continuation with the previous conference conducted by the Department Of Mechanical Engineering in the year 2014. The conference  will provide a forum for researchers from academia and industries from all over India, giving them an excellent opportunity to explore the role of smart technologies in the area of Manufacturing, Industrial, Design, Thermal and other areas related to Mechanical Engineering. The purpose of the conference is to present and discuss recent technological knowledge and share information in the relevant areas that could lead to future developments. The conference will provide an opportunity to present and observe the latest research ideas, developments and applications in the area of Mechanical Engineering. It will also provide opportunities to students to meet experienced peers.
										<br/><br/><b><u>RTME-2015 SCOPE</u></b><br/><br/>
The Conference is focused on the presentation/poster and discussion of research papers in the different areas of Mechanical Engineering and related fields.  Papers are invited from academia, technocrats, researchers and students on the following topics:
<br/><br/>
<b>Manufacturing and Industrial Engineering</b><br/><br/>
  Conventional & Non-Conventional Machining<br/>
  Intelligent Manufacturing Systems<br/>
  JIT, Lean, FMS and Agile Manufacturing<br/>
  Optimization<br/>
  Logistics and Supply Chain Management<br/>
  Ergonomics and Work System Design<br/>
  Quality & Reliability Engineering<br/>
  Engineering Materials<br/>
  Coating and Spray Technology<br/>
  Casting and welding process<br/>
  Any other relevant topics<br/><br/>
<b>Thermal Engineering</b><br/><br/>
  Developments in Aerospace Technology<br/>
  Biofuels and Internal Combustion Engines<br/>
  Bio-Thermal Engineering<br/>
  Green Energy Technology<br/>
  Analytical Techniques in Thermal Engineering<br/>
  Heat Transfer<br/>
  Cryogenic Engineering<br/>
  Recent Developments in HVAC systems<br/>
  Numerical Methods<br/>
  Thermodynamics of the Transport Phenomena<br/>
  Waste Management<br/>
  Any other relevant topics<br/><br/>
<b>Design and Dynamics</b><br/><br/>
  Computing in Applied Mechanics<br/>
  Dynamics and Control of Structures/ Systems<br/>
  Fracture and Failure Mechanics<br/>
  Biomechanics<br/>
  CAD, CAM , CAPP, CIM, CAE & Product Design<br/>
  Tool Design, Rapid Prototyping and tooling<br/>
  Modeling and Simulation<br/>
  Automotive and Transportation Systems<br/>
  Industrial Process Control and Robotics<br/>
  Rotor Dynamics<br/>
  Bond Graph Modeling<br/>
  Sensors and Instrumentation<br/>
  Vibration and Noise Control<br/>
  Any other relevant topics<br/><br/>
<b>Attractions</b><br/><br/>
• Keynote addresses by eminent personalities from reputed institutes and research organizations.<br/> 
• Exposure to state of art in Engineering research & development through exhibition of equipment, hardware and software related to Mechanical Engineering.

									</div>
								</div>
								<!-- ENDS posts -->
							</div>
							<!-- ENDS TABS -->		
					</div>
					<!-- ENDS content -->
				</div>
				<!-- ENDS wrapper-main -->
			</div>
			<!-- ENDS MAIN -->		
			<?php include "includes/footer.php"; ?>
	
	</body>
</html>