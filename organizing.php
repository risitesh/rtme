<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>National Advisory Committee | Recent Trends In Mechanical Enginnering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
					<!-- title -->
					<div id="page-title">
						<span class="title">Organizing Committee</span>
					</div>
					<!-- ENDS title -->

					<div class="one-column">
						<h6><u>CHIEF PATRON</u></h6>
						Dr. R.S. Khandpur,Former Director General, PGSC, Kapurthala cum Chairman BOG, BCET, Gurdaspur<br/><br/>
						<h6><u>PATRON</u></h6>
						Dr. Ravi Kumar, Principal, BCET Gurdaspur<br/><br/>
						<h6><u>CONFERENCE CHAIR</u></h6>
						Dr. Dilbag Singh, HOD (Mechanical Engineering)<br/><br/>
						<h6><u>CONFERENCE CO-CHAIR</u></h6>
						Dr. O.P. Singh, Professor, BCET Gurdaspur<br/>
						Dr. R.K. Awasthi, Professor, BCET Gurdaspur<br/><br/>
						<h6><u>ORGANIZING SECRETARY</u></h6>
						Dr. Harish Pungotra, Associate Professor, BCET Gurdaspur<br/>
						Dr. Nirmal S Kalsi, Associate Professor, BCET Gurdaspur<br/>
						<br/>
						<h6><u>CO-ORGANIZING SECRETARY</u></h6>
						Dr. Naveen Beri, Associate Professor, BCET Gurdaspur<br/>
						Dr. Brij Bhushan, Associate Professor, BCET Gurdaspur<br/><br/>
						<h6><u>ORGANIZING COMMITTEE MEMBERS</u></h6>
						Dr. Ranjit Singh, Associate Professor, BCET Gurdaspur<br/>
						Dr. Jagdev Singh, Associate Professor, BCET Gurdaspur<br/>
                                                Dr. Nripjit,Associate Professor, BCET Gurdaspur<br />
						Dr. Anil Mahajan, Associate Professor, BCET Gurdaspur<br/>
						Sh. Sunil Kumar, Associate Professor, BCET Gurdaspur<br/>
						Sh. Sanjeev Kumar, Associate Professor, BCET Gurdaspur<br/>
						Dr. Sarabjit Singh, Associate Professor, BCET Gurdaspur<br/>
						Dr. Simranpreet Singh Gill, Associate Professor, BCET Gurdaspur<br/>
						Sh. Preetkawal Singh, Assistant Professor, BCET Gurdaspur<br/>
                                                Sh. Jagjit Singh Maan, Assistant Professor, BCET Gurdaspur<br />
						Sh. Tarun Mahajan, Assistant Professor, BCET Gurdaspur<br/>
						Sh. Sandeep Gandotra, Assistant Professor, BCET Gurdaspur<br/>
						Sh. Arun Nanda, Assistant Professor, BCET Gurdaspur<br/>
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>