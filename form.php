<?php
include "includes/connect.php";
?>
<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Registration Form | Recent Trends In Mechanical Enginnering - 2015</title>
		
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<link rel="stylesheet" href="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
		
		

	</head>
	
	<body>

			<!-- HEADER -->
			<?php include "includes/header.php";?>
			<?php include "includes/nav.php";?>
			
			<div id="main">
				<!-- wrapper-main -->
				<div class="wrapper">
					
					
					<!-- content -->
					<div id="content">
						
					<!-- title -->
					<div id="page-title">
						<span class="title">Registration Form</span>
					</div>
					<!-- ENDS title -->

					<div class="one-column">
							<!-- form -->
							<form id="contactForm" action="form.php" method="post">
								<fieldset>
									<div>
										<label>Reference No.</label>
										<input name="ref" type="text" class="form-poshytip" title="Enter your Reference No." required />
									</div>				
									<p><input type="submit" value="Submit" name="submit" id="submit" /></p>
								</fieldset>
							</form>
							<?php
							if(isset($_POST['submit'])){
								$ref = mysql_real_escape_string($_POST['ref']);
								$_SESSION['ref'] = $ref;
								$q = mysql_query("SELECT * FROM paper WHERE id=".$ref) or die(mysql_error());
								if(mysql_num_rows($q)!=0){
									$qr = mysql_query("SELECT * FROM paper WHERE ab_approve=1 AND full_approve=1 AND camera_approve=1 AND id=".$ref) or die(mysql_error());
									if(mysql_num_rows($qr)!=0){
										echo "<a href='reg-form.php' target=_blank>Download Blank Registration Form</a><br />";
                                                                                echo "<a href='fill-form.php?ref=$ref' target=_blank>Fill Registration Form Online</a><br />";
                                                                                echo "<a href='download-reg.php?ref=$ref' target=_blank>Download Registration Form (If Alredy Registered Online)</a>";
									}else{
										echo "Paper Not Apprroved Yet";
									}
								}else{
									echo "Reference Number Not Found";
								}
							}
							?>
					</div>
				</div>
			</div>
			</div>
		
		
			<?php include "includes/footer.php";?>
	
	</body>
</html>