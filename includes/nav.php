<div id="menu">
  <div id="menu-holder">
    <div class="wrapper">
<nav>
<ul class="sf-menu" id="nav">
          <li class="selected"><a href="index.php">Home</a></li>
          <li><a href="#">Organizers</a>
            <ul>
              <li><a href="advisory.php">National Advisory Boards</a></li>
              <li><a href="organizing.php">Organizing Committes</a></li>
            </ul>
          </li>
          <li><a href="date.php">Important Dates</a></li>
          <li><a href="#">Paper Submission</a>
          <ul>
              <li><a href="abstract.php">Submit Your Abstract</a></li>
              <li><a href="full.php">Submit Your Full Paper</a></li>
              <li><a href="record.php">Submit Your Camera Ready Paper</a></li>
            </ul>
           </li>
          <li><a href="#">Conference Registration</a>
          <ul>
              <li><a href="fee.php">Registration Fee</a></li>
              <li><a href="form.php">Registration Form</a></li>
            </ul>
          </li>
    <li><a href="contact.php">Contact Us</a></li>
  </ul>
</nav>
</div>
          <!-- wrapper-menu -->
        </div>
        <!-- ENDS menu-holder -->
      </div>
      <!-- ENDS Menu -->