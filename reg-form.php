<?php
include 'includes/connect.php';
require('fpdf.php');
class PDF extends FPDF
{
	var $HREF;
	var $B;
	var $I;
	var $U;
	var $angle=0;

	function Rotate($angle,$x=-1,$y=-1)
	{
		if($x==-1)
			$x=$this->x;
		if($y==-1)
			$y=$this->y;
		if($this->angle!=0)
			$this->_out('Q');
		$this->angle=$angle;
		if($angle!=0)
		{
			$angle*=M_PI/180;
			$c=cos($angle);
			$s=sin($angle);
			$cx=$x*$this->k;
			$cy=($this->h-$y)*$this->k;
			$this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}

	function _endpage()
	{
		if($this->angle!=0)
		{
			$this->angle=0;
			$this->_out('Q');
		}
		parent::_endpage();
	}
// Page header
function Header()
{
    // Logo
    $this->Image('img/logo_BCET.jpg',10,12,20);
    // Arial bold 15
	$this->SetFont('Times','B',16);
	//$this->SetTextColor(159, 0, 197);
	$this->Ln(5);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,0,'BEANT COLLEGE OF ENGINEERING & TECHNOLOGY',0,0,'C');
	$this->Ln(8);
	$this->Cell(80);
	$this->Cell(30,0,'GURDASPUR-143521 (PB)',0,0,'C');
	$this->Ln(10);
	$this->Cell(80);
	$this->Cell(30,0,'2nd National Conference',0,0,'C');
	$this->Ln(8);
	$this->Cell(80);
	$this->Cell(30,0,'RECENT TRENDS IN MECHANICAL ENGINEERING (RTME-2015)',0,0,'C');
	$this->Ln(8);
	$this->Cell(80);
	$this->Cell(30,0,'March 20-21, 2015',0,0,'C');
	$this->Ln(8);
	$this->Cell(80);
	$this->Cell(30,0,'Registration Form',0,0,'C');
	//Line Break
	$this->Ln(12);
	$this->SetFont('Times','',50);
	$this->SetTextColor(220, 250, 220);
	$this->RotatedText(60,200,'RTME - 2015',35);
}
function RotatedText($x, $y, $txt, $angle)
{
	//Text rotated around its origin
	$this->Rotate($angle,$x,$y);
	$this->Text($x,$y,$txt);
	$this->Rotate(0);
}
function WriteHTML($html)
{
    // HTML parser
    $html = str_replace("\n",' ',$html);
    $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            // Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,$e);
        }
        else
        {
            // Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                // Extract attributes
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}
function WriteHTML1($html1)
{
    // HTML parser
    $html1 = str_replace("\n",' ',$html1);
    $a = preg_split('/<(.*)>/U',$html1,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            // Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,$e);
        }
        else
        {
            // Tag
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                // Extract attributes
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}
function OpenTag($tag, $attr)
{
    // Opening tag
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF = $attr['HREF'];
    if($tag=='BR')
        $this->Ln(4);
}

function CloseTag($tag)
{
    // Closing tag
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF = '';
}

function SetStyle($tag, $enable)
{
    // Modify style and select corresponding font
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach(array('B', 'I', 'U') as $s)
    {
        if($this->$s>0)
            $style .= $s;
    }
    $this->SetFont('',$style);
}
}
$html ="<b>1.	Name_______________________________________________________________________________<br><br>2.	Designation__________________________________________________________________________<br><br>3.	Field Of Specialization_________________________________________________________________<br><br>4.	Name Of Institute/ Organization_________________________________________________________<br><br>______________________________________________________________________________________<br><br>5.	Address For Communication____________________________________________________________<br><br>________________________________________________________________________________________<br><br>E-Mail Address__________________________________________________________________________<br><br>FAX____________________________________________________________________________________<br><br>Phone No.________________________________________________________________________________<br><br>6.	Highest Academic Qualification___________________________________________________________<br><br>7.	Type Of Participation	:		Paper/		Poster	Presentation/		Participation<br><br>8.	Title Of The Paper______________________________________________________________________<br><br>________________________________________________________________________________________<br><br>9.	Accomodation Required	:		Yes	/	No<br><br>10.	Details Of Draft	/	Money	Transfer<br><br>Name Of The Bank_____________________________________	Branch___________________________<br><br>Rs._______________________				Date	___	/	___	/	_____				DD No.	_______________________________<br><br><br>Place	:	_________________________________________																												Date	____	/	____	/	__________<br><br><br><br>																																																																																																																																									Signature Of Applicant<br><br><br>																																																																												Sponsorship Certificate<br><br>Prof.	/Dr.	/Ms.	/Mr. __________________________________________________ is an employee of our<br><br>institute 	/	department and is hereby sponsored to attend the conference<br><br><br><br><br>Seal																																																																																																																Signature	(Sponsoring Authority)</b>";
// Instanciation of inherited class
$pdf = new PDF();
$pdf->SetAutoPageBreak(false);
// Data loading
$pdf->SetTextColor(0, 0, 0);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetX(10);
$pdf->SetFont('Times','',10);
$pdf->SetLeftMargin(10);
$pdf->SetFontSize(10);
$pdf->SetFont('Times','',12);
$pdf->WriteHTML1($html);
$pdf->Output();
?>